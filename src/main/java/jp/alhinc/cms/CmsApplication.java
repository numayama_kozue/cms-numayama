package jp.alhinc.cms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import jp.alhinc.cms.entity.User;
import jp.alhinc.cms.form.RegisterUserForm;
import jp.alhinc.cms.service.users.CreateUserService;
import jp.alhinc.cms.service.users.FindUserService;

@SpringBootApplication
public class CmsApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(CmsApplication.class, args);
	}

	@Autowired
	private FindUserService findUserService;

	@Autowired
	private CreateUserService createUserService;

	@Override
	public void run(String... args) throws Exception {
		User admin = findUserService.findByLoginId("admin");

		if (admin == null) {
			RegisterUserForm form = new RegisterUserForm();
			form.setLoginId("admin");
			form.setName("システム管理者");
			form.setNameKana("システムカンリシャ");
			form.setRawPassword("Zaq12wsx");
			form.setBranchId(1L);
			form.setDepartmentId(1L);
			form.setPositionId(1L);
			createUserService.create(form);
		}
	}

}
