package jp.alhinc.cms.common;

import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
public final class MessageUtil {

	static MessageSource instance;

	@Autowired
	private MessageSource source;

	@PostConstruct
	private void init() {
		MessageUtil.instance = source;
	}

	/**
	 * メッセージIDに該当するメッセージを返却します.
	 * @param id メッセージID
	 * @return メッセージ
	 */
	public static String getMessage(MessageId id) {
		return instance.getMessage(id.toString(), new Object[]{}, Locale.getDefault());
	}

	/**
	 * メッセージIDに該当するメッセージを取得し、パラメータの文字列配列の値を埋め込んだメッセージを返却します.
	 * @param id メッセージID
	 * @param embededStrings 埋め込み文字列配列
	 * @return メッセージ
	 */
	public static String getMessage(MessageId id, String... embededStrings) {
		return instance.getMessage(id.toString(), embededStrings, Locale.getDefault());
	}

}
