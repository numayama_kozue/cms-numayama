package jp.alhinc.cms.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.alhinc.cms.entity.User;
import jp.alhinc.cms.form.me.ChangePasswordForm;
import jp.alhinc.cms.service.me.ChangePasswordService;

@Controller
@RequestMapping("/me")
public class MeController extends AuthenticatedController {

	@Autowired
	private ChangePasswordService service;

	@GetMapping("/password")
	public String password(Model model) {
		model.addAttribute("form", new ChangePasswordForm());
		return "me/password";
	}

	@PatchMapping("/password")
	public String changePassword(@Valid @ModelAttribute("form") ChangePasswordForm form, BindingResult result, Model model) {
		User currentUser = getCurrentUser();
		System.out.println("current" + form.getCurrentPassword());
		System.out.println("new" + form.getNewPassword());
		System.out.println("confirm" + form.getPasswordConfirmation());

		if (result.hasErrors()) {
			model.addAttribute("form", form);
			System.out.println("cccccccccc");
			return "me/password";
		}

		int password = service.updatePassword(currentUser.getId(), form);
		if (password == 0) {
			model.addAttribute("form", form);
			model.addAttribute("message","INF_010");
			return "me/password";
		}
		return "/";
	}
}
