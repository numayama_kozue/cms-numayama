package jp.alhinc.cms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/approvals")
public class ApprovalsController extends AuthenticatedController {

	@GetMapping
	public String index() {
		return "approvals/index";
	}
}
