package jp.alhinc.cms.form.me;

import java.io.Serializable;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import jp.alhinc.cms.validator.user.Password;

public class ChangePasswordForm implements Serializable {

	//現在のパスワード
	@NotEmpty(message = "INF_002")
	@Pattern(regexp = "^[!\\\"#$%&‘()*+,-./:;<=>?@[¥]^_`{|}~]$", message = "INF_005")
	@Size(min = 8, max = 20, message = "INF_012")
	@Password(message = "INF_007")
	private String currentPassword;

	//新パスワード
	@AssertTrue(message = "INF_008")
	@NotBlank(message = "INF_002")
	@Pattern(regexp = "^[!\\\"#$%&‘()*+,-./:;<=>?@[¥]^_`{|}~]$", message = "INF_005")
	@Size(min = 8, max = 20, message = "INF_012")
	@Password(message = "INF_007")
	private String newPassword;
	public boolean newPasswordValid() {
		if(newPassword == null || newPassword.isEmpty())
			return true;
		return !(newPassword.equals(currentPassword));
	}

	//確認用パスワード
	@AssertTrue(message = "INF_011")
	@NotBlank(message = "INF_002")
	private String passwordConfirmation;
	public boolean passwordConfirmationValid() {
		if(newPassword == null || newPassword.isEmpty())
			return true;
		return newPassword.equals(passwordConfirmation);
	}

	/**
	 * @return currentPassword
	 */
	public String getCurrentPassword() {
		return currentPassword;
	}

	/**
	 * @param currentPassword セットする currentPassword
	 */
	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	/**
	 * @return newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param newPassword セットする newPassword
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * @return passwordConfirmation
	 */
	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	/**
	 * @param passwordConfirmation セットする passwordConfirmation
	 */
	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}
}
