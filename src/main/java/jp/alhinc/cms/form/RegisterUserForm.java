package jp.alhinc.cms.form;

public class RegisterUserForm {

	private String loginId;

	private String name;

	private String nameKana;

	private String rawPassword;

	private Long branchId;

	private Long departmentId;

	private Long positionId;

	/**
	 * @return loginId
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * @param loginId セットする loginId
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return rawPassword
	 */
	public String getRawPassword() {
		return rawPassword;
	}

	/**
	 * @param rawPassword セットする rawPassword
	 */
	public void setRawPassword(String rawPassword) {
		this.rawPassword = rawPassword;
	}

	/**
	 * @return departmentId
	 */
	public Long getDepartmentId() {
		return departmentId;
	}

	/**
	 * @param departmentId セットする departmentId
	 */
	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return positionId
	 */
	public Long getPositionId() {
		return positionId;
	}

	/**
	 * @param positionId セットする positionId
	 */
	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	/**
	 * @return nameKana
	 */
	public String getNameKana() {
		return nameKana;
	}

	/**
	 * @param nameKana セットする nameKana
	 */
	public void setNameKana(String nameKana) {
		this.nameKana = nameKana;
	}

	/**
	 * @return branchId
	 */
	public Long getBranchId() {
		return branchId;
	}

	/**
	 * @param branchId セットする branchId
	 */
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

}
