package jp.alhinc.cms.service.me;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.cms.entity.User;
import jp.alhinc.cms.form.me.ChangePasswordForm;
import jp.alhinc.cms.mapper.UserMapper;

@Service
public class ChangePasswordService {

	@Autowired
	private UserMapper mapper;

	@Transactional
	public int updatePassword(Long id, ChangePasswordForm form) {
		User target = mapper.findById(id);
		//ログイン中のUser情報
		System.out.println("fffffffff");
		BCryptPasswordEncoder bCryptPassword = new BCryptPasswordEncoder();
		if (!bCryptPassword.matches(form.getNewPassword(),target.getPassword())) {
			// TODO : エラーコード返す
			return 0;
		}
		return mapper.updatePassword(id, new BCryptPasswordEncoder().encode(form.getNewPassword()));
	}
}
