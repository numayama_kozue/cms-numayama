package jp.alhinc.cms.service.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.cms.entity.User;
import jp.alhinc.cms.form.RegisterUserForm;
import jp.alhinc.cms.mapper.UserMapper;

@Service
public class CreateUserService {

	@Autowired
	private UserMapper mapper;

	@Transactional
	public void create(RegisterUserForm form) {
		User entity = new User();
		entity.setLoginId(form.getLoginId());
		entity.setName(form.getName());
		entity.setPassword(new BCryptPasswordEncoder().encode(form.getRawPassword()));

		mapper.insert(entity);
	}
}
